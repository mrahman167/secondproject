package secondpackage;

public class ConstructorPlane {
    public ConstructorPlane(String plane1, String plane2, String plane3, int numberOfPlanes) {
        planeType1 = plane1;
        planeType2 = plane2;
        planeType3 = plane3;
        totalnumberOfPlanes = numberOfPlanes;


    }

    public String planeType1;
    public String planeType2;
    public String planeType3;
    public int totalnumberOfPlanes;

    public void printPlaneType1() {
        System.out.println(planeType1);
    }

    public void printPlaneType2() {
        System.out.println(planeType2);
    }

    public void printPlaneType3() {
        System.out.println(planeType3);
    }

    public void printtotalofplanes() {
        System.out.println(totalnumberOfPlanes);
    }
}
