package secondpackage;

public class Participants {

    public Participants(String person1, String person2, String person3, int totalpersons) {
        participant1 = person1;
        participant2 = person2;
        participant3 = person3;
        total = totalpersons;

    }

    public String participant1;
    public String participant2;
    public String participant3;
    public int total;

    public void printParticipant1() {
        System.out.println(participant1);
    }

    public void printparticipant2() {
        System.out.println(participant2);
    }

    public void printParticipant3() {
        System.out.println(participant3);
    }

    public void printTotal() {
        System.out.println(total);
    }

}
